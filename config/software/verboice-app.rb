name "verboice-app"
version "2.1"

dependency "ruby"
dependency "rubygems"
dependency "bundler"
dependency 'erlang'
dependency 'mysql'
dependency 'zeromq'
dependency 'libxslt'
dependency 'libxml2'
dependency 'nodejs'
dependency 'sox'
# dependency "rsync"

source :git => "https://bitbucket.org/instedd/verboice.git"

relative_path "verboice"

# env = {
#   "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}"
# }

env = {
  "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}",
  "LDFLAGS" => "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include",
  "CFLAGS" => "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include",
  "LD_RUN_PATH" => "#{install_dir}/embedded/lib"
}

build do
  bundle "install --path=.bundle --without development test"
  rake "db:setup RAILS_ENV=production", env: env
  rake "assets:precompile", env: env

  gem "install passenger --no-ri --no-rdoc"
  ruby "#{install_dir}/embedded/bin/passenger-config build-native-support"
  command "make -C broker", env: env
  command "mkdir -p #{install_dir}/embedded/service/verboice"
  command "rsync -a --delete --exclude=.git/*** --exclude=.gitignore ./ #{install_dir}/embedded/service/verboice/"
end
