name "mysql"
default_version "5.6.17"

version "5.6.17" do
  source :md5 => '82114fa7c13fa3ca897b34666577d9f4',
         :url => 'http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.17.tar.gz'
end

relative_path "mysql-#{version}"


env = {
  "CFLAGS" => "-L#{install_dir}/embedded/lib",
  "LDFLAGS" => "-Wl,-rpath #{install_dir}/embedded/lib -L#{install_dir}/embedded/lib"
}

build do
  # # set up the erlang include dir
  # command "mkdir -p #{install_dir}/embedded/erlang/include"
  # %w{ncurses openssl zlib.h zconf.h}.each do |link|
  #   command "ln -fs #{install_dir}/embedded/include/#{link} #{install_dir}/embedded/erlang/include/#{link}"
  # end
  command "cmake -DCMAKE_INSTALL_PREFIX:PATH=#{install_dir}/embedded -DINSTALL_MYSQLTESTDIR:STRING= .", :env => env
  command "make -C libmysql -j #{max_build_jobs}", :env => env
  command "make -C libmysql install"
  command "make -C include install"

  # command(["./configure",
  #          "--prefix=#{install_dir}/embedded",
  #         ].join(" "),
  #         :env => env)

  # command "make -j #{max_build_jobs}", :env => env
  # command "make install"
end
