name "libuuid"
default_version "1.0.2"

version "1.0.2" do
  source md5: "c06f49871e9d964d6bbf8688f57aa124"
end

source url: "http://ufpr.dl.sourceforge.net/project/libuuid/libuuid-#{version}.tar.gz"

relative_path "libuuid-#{version}"

build do
  command "./configure --prefix=#{install_dir}/embedded"
  command "make -j #{max_build_jobs}"
  command "make install"
end
