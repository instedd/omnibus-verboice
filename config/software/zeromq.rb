name "zeromq"
default_version "2.2.0"

dependency "libuuid"

version "2.2.0" do
  source md5: "1b11aae09b19d18276d0717b2ea288f6"
end

version "3.2.4" do
  source md5: "39af2d41e3fb744b98d7999e291e05dc"
end

source url: "http://download.zeromq.org/zeromq-#{version}.tar.gz"

relative_path "zeromq-#{version}"

env = {
  # "CFLAGS" => "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include",
  "CPPFLAGS" => "-I#{install_dir}/embedded/include",
  "LDFLAGS" => "-L#{install_dir}/embedded/lib"
}

build do
  command "./configure --prefix=#{install_dir}/embedded", env: env
  command "make -j #{max_build_jobs}", env: env
  command "make install"
end
