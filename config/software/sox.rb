name "sox"
version "14.4.1"

source url: "http://softlayer-dal.dl.sourceforge.net/project/sox/sox/14.4.1/sox-14.4.1.tar.gz",
       md5: "670307f40763490a2bc0d1f322071e7a"

relative_path "sox-#{version}"

build do
  command "./configure --prefix=#{install_dir}/embedded --disable-gomp"
  command "make -j #{max_build_jobs}"
  command "make install"
end
