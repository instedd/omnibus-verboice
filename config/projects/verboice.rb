
name 'verboice'
maintainer 'CHANGE ME'
homepage 'CHANGEME.com'

replaces        'verboice'
install_path    '/opt/verboice'
build_version   Omnibus::BuildVersion.new.semver
build_iteration 1

# creates required build directories
dependency 'preparation'

# verboice dependencies/components
# dependency 'somedep'

override :ruby, version: '1.9.3-p484'
override :erlang, version: 'R16B03-1'
override :bundler, version: '1.5.3'

# version manifest file
dependency 'version-manifest'

dependency 'verboice-app'

exclude '\.git*'
exclude 'bundler\/git'
